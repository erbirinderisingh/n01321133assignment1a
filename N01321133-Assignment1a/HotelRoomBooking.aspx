﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelRoomBooking.aspx.cs" Inherits="N01321133_Assignment1a.HotelRoomBooking" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hotel Room Booking Form</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            
            <asp:ValidationSummary id="ValidationSummary1" Runat="server" />

            <label>Enter Client Name</label><ASP:Textbox runat="server" ID="clientName" placeholder="Enter Client Name"></ASP:Textbox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="clientName" ID="clientNameValidator" ErrorMessage="Please Enter Client Name"></asp:RequiredFieldValidator>
            <br/>
            <label>Client Email</label><ASP:Textbox runat="server" ID="clientEmail" placeholder="Enter Client Email"></ASP:Textbox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="clientEmail" ID="clientEmailValidator" ErrorMessage="Please Enter Client Email"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br/>
            <label>Meals Required</label> 
            <div id="meals" runat="server">
            <asp:CheckBox runat="server" ID="breakfast" Text="Breakfast" />
            <asp:CheckBox runat="server" ID="lunch" Text="Lunch" />
            <asp:CheckBox runat="server" ID="dinner" Text="Dinner" />
            </div>
            <br />
            <label>Check In Date</label><ASP:Calendar runat="server" ID="checkInDate" placeholder="Enter CheckInDate"></ASP:Calendar>
            <br/>
            <label>Check In Time</label>
            <asp:TextBox runat="server" ID="checkInTime" placeholder="CheckInTime Time (24 hour format)"></asp:TextBox>
            <asp:RangeValidator EnableClientScript="false" ID="checkInTimeValidator" runat="server" ControlToValidate="checkInTime" MinimumValue="600" Type="Integer" MaximumValue="1200" ErrorMessage="CheckIn Time can be between 6am and 12Noon"></asp:RangeValidator>
            <br/>
            <br />
            <label>Room Type</label>
            <asp:DropDownList runat="server" ID="roomType">
                <asp:ListItem Value="" Text="Select Room"></asp:ListItem>
                <asp:ListItem Value="S" Text="Standard"></asp:ListItem>
                <asp:ListItem Value="D" Text="Deluxe"></asp:ListItem>
                <asp:ListItem Value="U" Text="Suite"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="roomType" ID="roomTypeValidator" ErrorMessage="Please Select Room Type"></asp:RequiredFieldValidator>
            
            <br/>
            <ASP:button runat="server" ID="submit" Text="Submit"></ASP:button>
        </div>
    </form>
</body>
</html>
